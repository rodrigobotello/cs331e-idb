#!/usr/bin/env python3


# ---------------------------

#!/usr/bin/env python3

# ---------------------------

# ---------------------------

from flask import Flask, render_template, request
from sqlalchemy import create_engine, MetaData, func
from sqlalchemy.orm import sessionmaker
from create_db import Genre,db, Artist, Venue, Show, artist, genre, venue, app
from os import path
import dateutil.parser as pdp
from datetime import datetime


#app = Flask(__name__)

#engine = create_engine('sqlite:///sql_db.db')
#db.metadata.bind = engine
#DBSession = sessionmaker(bind=engine)
#session = DBSession()
# -----------
# dictionaries
# -----------
def venue_dict():
    d = db.session.query(Venue)
    venues = {}
    for i in d:
        venues[i.venue_id] = [
        i.name,
        i.address,
        [show_parser(j, False) for j in i.venue_shows],
        i.images]

        # Make the url the ID instead of the name, because it's unique
        #venues[i.name.replace(' ', '').lower()] = [i.name, i.address]
    return venues


def genre_dict():
    d = db.session.query(Genre)

    artists = [(i.artists) for i in d]
    images =[]
    for i in artists[0]:
        images.append(i.images)

    genres = {}
    for i in d:

        genres[i.genre_id] = [     # key is the ID
        i.name,
        [j[1] for j in i.subgenres],                    # pulls the names of all subgenres
        [j.name for j in i.artists],                    # pulls the names of all artists
        [j.artist_id for j in i.artists],
        images[0]]

        #print(i.images)


        # genres[i.name.replace(' ', '').lower()] = [     # key is the lowercase, spaceless name
        # i.name,
        # [j[1] for j in i.subgenres],                    # pulls the names of all subgenres
        # [j.name for j in i.artists]]                    # pulls the names of all artists
    return genres

def musician_dict():
    d = db.session.query(Artist)
    musicians = {}
    for i in d:
        musicians[i.artist_id] = [                      # key is the ID
        i.name,
        [j.name for j in i.genres],                     # pulls the names of all genres
        i.totalNumUpcoming,                             # pulls number of upcoming concerts
        [show_parser(j, True) for j in i.artist_shows],
        i.images]

        # musicians[i.name.replace(' ', '').lower()] = [  # key is the lowercase, spaceless name
        # i.name,
        # [j.name for j in i.genres],                     # pulls the names of all genres
        # i.totalNumUpcoming]                             # pulls number of upcoming concerts
    return musicians
def show_parser(i, artist):
    if artist:
        data = db.session.query(Venue).filter(Venue.venue_id==i.venue_id).one()
        relevantID = i.venue_id
    else:
        data = db.session.query(Artist).filter(Artist.artist_id==i.artist_id).one()
        relevantID = i.artist_id

    try:
        saleStart = pdp.isoparse(i.saleStart).strftime("%A, %B %d, %Y %I:%M%p")
    except:
        saleStart = i.saleStart
    try:
        presaleStart = pdp.isoparse(i.presaleStart).strftime("%A, %B %d, %Y %I:%M%p")
    except:
        presaleStart = i.presaleStart
    try:
        doorsOpen = datetime.strptime(i.doorsOpen, "%H:%M:%S").strftime("%I:%M %p")
    except:
        doorsOpen = i.doorsOpen
    try:
        startTime = datetime.strptime(i.startTime, "%H:%M:%S").strftime("%I:%M %p")
    except:
        startTime = i.startTime
    thisShow = [pdp.isoparse(i.date).strftime("%A, %B %d, %Y %I:%M%p"),\
    data.name, saleStart, presaleStart, i.priceMin, doorsOpen, i.restricted,\
    startTime, i.url, relevantID]
    return thisShow

venues = venue_dict()
genres = genre_dict()
musicians = musician_dict()

# registers the "top" menubar

# ------------
# index
# ------------
@app.route('/')
def index():
    if request.method == 'POST':

        search = request.args.get('search')  # try this instead
        return redirect(url_for('search', search=search))
    return render_template('index.html')

# ------------
# pillars
# ------------
@app.route('/venue/', methods=['GET','POST'])
def venue():
    col_list = ['Name', 'Address']              # table headers
    item_list = [[i[0], i[1]] for i in venues.values()]  # table values
    venue_list = [key for key in venues.keys()]
    return render_template('venues/venue.html',
    columns = col_list,
    items = item_list,
    venues = venue_list)

@app.route('/genre/', methods=['GET','POST'])
def genre():
    col_list = ['Name', 'Artist', 'Artist', 'Artist', 'Subgenre', 'Subgenre']   # table headers
    item_list = []
    genre_list = [key for key in genres.keys()]
    for i in genres.values():    # table values
        row = [i[0]]    # name values
        for j in range(3):  # add 3 artists, leave blank values in table if neccessary
            try:
                row.append(i[2][j])
            except:
                row.append('')
        for j in range(2):  # add 2 subgenres, leave blank values in table if necessary
            try:
                row.append(i[1][j])
            except:
                row.append('')
        item_list.append(row)
    return render_template('genres/genre.html',
    columns = col_list,
    items = item_list,
    genres = genre_list)

@app.route('/musicians/', methods=['GET','POST'])
def musician():
    col_list = ['Name', 'Genre', 'Planned Concerts']  # table headers
    item_list = [[i[0], i[1][0], i[2]] for i in musicians.values()]
    music_list = [key for key in musicians.keys()]
    return render_template('musicians/musician.html',
    columns = col_list,
    items = item_list,
    music = music_list,
    upcoming_shows = item_list[3])

@app.route('/models/', methods=['GET','POST'])
def models():
    col_list = ['flower', 'color', 'season']
    item_list = [['tulip', 'yellow', 'winter'], ['rose', 'white', 'summer']]
    return render_template(
    'models.html',
    columns = col_list,
    items = item_list
    )

@app.route('/about/', methods=['GET','POST'])
def about():
    return render_template('about.html')

@app.route('/', methods=['GET','POST'])
def live():
    return render_template('index.html')

# -----------
# subpages
# -----------

@app.route('/venues/<venue>/', methods=['GET', 'POST'])
def venue_template(venue):
    url = '/venues/{}.html'.format(venue)
    print(venues[venue][3])
    if venues[venue][3]=='none':
        venues[venue][3]='https://static1.colliderimages.com/wordpress/wp-content/uploads/2021/02/rick-astley-social.jpg'
    if not path.exists(url):                    # create a new venue page if necessary
        f = open('templates/' + url, "w")
        f.write("{% extends 'venues/venues_template.html' %}")
        f.close()
    return render_template(         # pass in the url and relevant information
        url,
        name = venues[venue][0],
        address = venues[venue][1],
        shows = venues[venue][2],
        image = venues[venue][3],
        show_columns = ['Date', 'Artist', 'Sale Start',\
         'Presale Start', 'Tickets Starting At', 'Doors Open', 'Restricted', 'Start Time', 'Ticketmaster URL']
    )

@app.route('/genres/<genre>/', methods=['GET', 'POST'])
def genre_template(genre):
    # if genres[genre][4][0]['url']:
    #     genre[genre][4][0]['url'] = 'https://static1.colliderimages.com/wordpress/wp-content/uploads/2021/02/rick-astley-social.jpg'
    # 
    # for i in venue[venue][4][0]['url']
    url = '/genres/{}.html'.format(genre)
    if not path.exists(url):                    # create a new genre page if necessary
        f = open('templates/' + url, "w")
        f.write("{% extends 'genres/genre_template.html' %}")
        f.close()
    return render_template(         # pass in the url and relevant information
        url,
        name = genres[genre][0],
        musicians = ', '.join(genres[genre][2]),    # converts this list into a readable string
        subgenres = ', '.join(genres[genre][1]),
        artist_ids = genres[genre][3],
        artist_names =genres[genre][2],
        columns= ['Artist'],
        image = genres[genre][4][0]['url']#, 'Subgenre']
    )

@app.route('/musicians/<musician>/', methods=['GET', 'POST'])
def musician_template(musician):
    #print(musicians[musician][4][0]['url'])
    url = '/musicians/{}.html'.format(musician)  # url without whitespaces
    if not path.exists(url):                    # create a new musicians page if necessary
        f = open('templates/' + url, "w")
        f.write("{% extends'musicians/musician_template.html' %}")
        f.close()
    return render_template(         # pass in the url and relevant information
        url,
        name = musicians[musician][0],
        genres = ', '.join(musicians[musician][1]),
        num_shows = musicians[musician][2],
        showKeys = musicians[musician][3],
        shows = musicians[musician][3],
        image = musicians[musician][4][0]['url'],
        show_columns = ['Date', 'Venue', 'Sale Start',\
         'Presale Start', 'Tickets Starting At', 'Doors Open', 'Restricted', 'Start Time', 'Ticketmaster URL']
    )

@app.route('/search/', methods=['GET', 'POST'])
def search():
    if request.method == 'POST':
        search = request.form['search']

        url = '/search/{}.html'.format(search.lower())  # url without whitespaces
        if not path.exists(url):                    # create a new musicians page if necessary
            f = open('templates/' + url, "w")
            f.write("{% extends'search/search_results.html' %}")
            f.close()

        artist = db.session.query(Artist).filter(func.lower(Artist.name).contains(search.lower())).all()
        artist = [[i.name, i.artist_id] for i in artist]
        genre = db.session.query(Genre).filter(func.lower(Genre.name).contains(search.lower())).all()
        genre = [[i.name, i.genre_id] for i in genre]
        venue = db.session.query(Venue).filter(func.lower(Venue.name).contains(search.lower())).all()
        venue = [[i.name, i.venue_id] for i in venue]
        return render_template(url, artist=artist, venue=venue, genre=genre, columns=['Artists', 'Genres', 'Venues'])
    else:
        return render_template('/search/search.html')


# debug=True to avoid restart the local development server manually after each change to your code.
# host='0.0.0.0' to make the server publicly available.
if __name__ == "__main__":
    app.run(host='localhost', port='5000',debug=False)
	#app.run(debug=True, host='127.0.0.1')

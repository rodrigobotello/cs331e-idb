import os
import sys
from unittest import main, TestCase
from models import app, db, Genre, Artist, Venue, Show

#There are currently 3 artist test cases (one being edge case)
# 3 regular genre tests (one edge case), one unique genre test
# 3 venue tests :) we need one to be a real venue that exists -matching every character
#for an edge case so let's do that tonight

class DBTestCases(TestCase):

    def test_query_artist_1(self):
        d = db.session.query(Artist)
        artists = [i.name for i in d]
        numUpcoming = [i.totalNumUpcoming for i in d]
        shows = [i.artist_shows for i in d]

        for i in range(len(artists)):
            for k in shows[i]:
                venue = db.session.query(Venue).filter(Venue.venue_id==k.venue_id).one()


    def test_insert_genre_2(self):
        # Delete if it already exists
        db.session.query(Genre).filter_by(genre_id = '-1').delete()

        # Insert into Genre
        thisGenre = Genre(genre_id='-1', name='Electronic')
        db.session.add(thisGenre)
        db.session.add(thisGenre)

        # Query Genre and assert the id
        r = db.session.query(Genre).filter_by(genre_id = '-1').one()
        self.assertEqual(str(r.genre_id), '-1')

        # Delete the inputed value
        db.session.query(Genre).filter_by(genre_id = '-1').delete()
        db.session.commit()

    def test_insert_genre_3(self):
        # Delete if it already exists
        db.session.query(Genre).filter_by(genre_id = '300').delete()

        # Insert into Genre
        thisGenre = Genre(genre_id='300', name='Pop')
        db.session.add(thisGenre)
        db.session.add(thisGenre)

        # Query Genre and assert the id
        r = db.session.query(Genre).filter_by(genre_id = '300').one()
        self.assertEqual(str(r.genre_id), '300')

        # Delete the inputed value
        db.session.query(Genre).filter_by(genre_id = '300').delete()
        db.session.commit()

    def test_unique_genre_4(self):
        # Create list
        unique = []

        # Check Ids
        for row in db.session.query(Genre):
            unique.append(row.genre_id)

        # Test if unique
        self.assertEqual(len(set(unique)), len(unique))

    def test_insert_artist_5(self):
        # Delete if it already exists
        db.session.query(Artist).filter_by(artist_id = '6969').delete()

        # Create Artist Object and Update
        thisArtist = Artist(artist_id='6969', name='Rodrigo Botello')
        db.session.add(thisArtist)
        db.session.commit()

        # Query Genre and assert the id
        r = db.session.query(Artist).filter_by(artist_id = '6969').one()
        self.assertEqual(str(r.artist_id), '6969')

        # Delete the inputed value
        db.session.query(Artist).filter_by(artist_id = '6969').delete()
        db.session.commit()


    def test_insert_artist_69(self):
        # Delete if it already exists
        db.session.query(Artist).filter_by(artist_id = '9999').delete()

        # Create Artist Object and Update
        thisArtist = Artist(artist_id='9999', name='Sam Miles')
        db.session.add(thisArtist)
        db.session.commit()

        # Query Genre and assert the id
        r = db.session.query(Artist).filter_by(artist_id = '9999').one()
        self.assertEqual(str(r.artist_id), '9999')

        # Delete the inputed value
        db.session.query(Artist).filter_by(artist_id = '9999').delete()
        db.session.commit()

    #an atypical test case testing an id that likely already exists
    def test_insert_artist_6(self):
        # Delete if it already exists
        db.session.query(Artist).filter_by(artist_id = '1').delete()

        # Create Artist Object and Update
        thisArtist = Artist(artist_id='1', name='Fares Fraij')
        db.session.add(thisArtist)
        db.session.commit()

        # Query Genre and assert the id
        r = db.session.query(Artist).filter_by(artist_id = '1').one()
        self.assertEqual(str(r.artist_id), '1')

        # Delete the inputed value
        db.session.query(Artist).filter_by(artist_id = '1').delete()
        db.session.commit()

    #this DB value should already exist too, just trying to see if our code
    #will replace it with new stuff
    def test_insert_genre_7(self):
        # Delete if it already exists
        db.session.query(Genre).filter_by(genre_id = '4').delete()

        # Insert into Genre
        thisGenre = Genre(genre_id='4', name='Electronic')
        db.session.add(thisGenre)
        db.session.add(thisGenre)

        # Query Genre and assert the id
        r = db.session.query(Genre).filter_by(genre_id = '4').one()
        self.assertEqual(str(r.genre_id), '4')

        # Delete the inputed value
        db.session.query(Genre).filter_by(genre_id = '4').delete()
        db.session.commit()


    #did not double check that venue_id is what I should be calling
    #or the possible ID values but those are easy fixes
    #one of these should be a venue that actually exists lol
    def test_insert_venue1(self):
        # Delete if it already exists
        db.session.query(Venue).filter_by(venue_id = '4').delete()

        # Insert into Venue
        thisVenue = Venue(venue_id='4', name='Moody')
        db.session.add(thisVenue)
        db.session.add(thisVenue)

        # Query Venue and assert the id
        r = db.session.query(Venue).filter_by(venue_id = '4').one()
        self.assertEqual(str(r.venue_id), '4')

        # Delete the inputed value
        db.session.query(Venue).filter_by(venue_id = '-1').delete()
        db.session.commit()

    def test_insert_venue2(self):
        # Delete if it already exists
        db.session.query(Venue).filter_by(venue_id = '9999').delete()

        # Insert into Venue
        thisVenue = Venue(venue_id='9999', name='Central Market South')
        db.session.add(thisVenue)
        db.session.add(thisVenue)

        # Query Venue and assert the id
        r = db.session.query(Venue).filter_by(venue_id = '9999').one()
        self.assertEqual(str(r.venue_id), '9999')

        # Delete the inputed value
        db.session.query(Venue).filter_by(venue_id = '9999').delete()
        db.session.commit()

    def test_insert_venue3(self):
        # Delete if it already exists
        db.session.query(Venue).filter_by(venue_id = '14').delete()

        # Insert into Venue
        thisVenue = Venue(venue_id='14', name='Central Market North')
        db.session.add(thisVenue)
        db.session.add(thisVenue)

        # Query Venue and assert the id
        r = db.session.query(Venue).filter_by(venue_id = '14').one()
        self.assertEqual(str(r.venue_id), '14')

        # Delete the inputed value
        db.session.query(Venue).filter_by(venue_id = '14').delete()
        db.session.commit()



if __name__ == "__main__":
    main()

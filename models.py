# This basically takes the role of sql.py, but I decided to leave the other
# One thing to note is that this will give you an error if you don't have
# the postgres database 'bookdb' on your machine. We will probably need to change
# that address to be whatever like maps to the actual website?
# But yeah
# - <3 Anna
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import requests
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:asd123@localhost:5432/bookdb')
#switch lines for gcp
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql+psycopg2://postgres:cs331e@/postgres?host=/cloudsql/cs331e-idb-306017:us-central1:sqldb')

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
app.config['DEBUG']=True
db = SQLAlchemy(app)


genre_artist = db.Table('genre_artist', db.Model.metadata,
    db.Column('genre_id', db.String(40), db.ForeignKey('genre.genre_id')),
    db.Column('artist_id', db.String(40), db.ForeignKey('artist.artist_id'))
)

class Genre(db.Model):
    # Genre may have many artists
    __tablename__= 'genre'
    # genre_id is a unique string which can be used to look up genres.
    genre_id = db.Column(db.String(40), primary_key=True)
    name = db.Column(db.String(250), nullable=False)

    # images is a list of dictionaries. Each dictionary is a dictionary containing
    # info about an image of an artist in the genre (including the url).
    images = db.Column(db.PickleType, nullable=True)

    # List containing the names of subgenres. Probably don't worry too much about
    # incorporating this at this stage.
    subgenres = db.Column(db.PickleType, nullable=True)

    # List of artists within this genre.
    artists = db.relationship('Artist', secondary=genre_artist)


class Artist(db.Model):
    # Artist may have many genres
    # Artist may have many events at different venues
    __tablename__= 'artist'
    artist_id = db.Column(db.String(40), primary_key=True) # unique string
    name = db.Column(db.String(250), nullable=False)

    # List of genres for an artist; currently only contains their 'primary' genre.
    genres = db.relationship('Genre', secondary=genre_artist)

    # Associates artists with venues through shows.
    artist_shows = db.relationship('Show', back_populates='venue_shows')

    # A dictionary containing social media links.
    # Example usage: artist.socMed['twitter'][0]['url'] gives the twitter URL.
    # Keep in mind that this may be empty for some artists.
    socMed = db.Column(db.PickleType, nullable=True)

    # List of dictionaries containining info about images of the artist, including the images' URLs.
    images = db.Column(db.PickleType, nullable=True)

    # The total number of shows the artist has planned, regardless of whether they are in Austin.
    totalNumUpcoming=db.Column(db.String(10), nullable=True)
    def genre(self):
        return [i.name for i in self.genres]
    def get_shows(self):
        return [i.name for i in self.shows]

class Venue(db.Model):
    __tablename__ = 'venue'
    venue_id = db.Column(db.String(40), primary_key=True) # Unique string
    name = db.Column(db.String(250), nullable=False)
    address = db.Column(db.String(250))
    post_code = db.Column(db.String(250))

    # List of dictionaries containining info about images of the venue, including the images' URLs.
    images = db.Column(db.PickleType, nullable=True)

    # Associates artists with artists through shows.
    venue_shows = db.relationship('Show', back_populates='artist_shows')

    def get_shows(self):
        return [i.artist_name for i in self.venue_shows]

class Show(db.Model):
    __tablename__='show'
    show_id=db.Column(db.String(40), primary_key=True)
    venue_id=db.Column(db.String(40), db.ForeignKey('venue.venue_id'))
    artist_id=db.Column(db.String(40), db.ForeignKey('artist.artist_id'))
    artist_name=db.Column(db.String(250))


    date = db.Column(db.String(40))#Data of show

    saleStart = db.Column(db.String(40))#Date of ticket sales start

    presaleStart = db.Column(db.String(60)) # Date of presale start, if applicable

    priceMin= db.Column(db.String(40))# Minimum price of a ticket

    doorsOpen=db.Column(db.String(40))# Doors open time


    restricted=db.Column(db.String(10))# Whether or not the show is 18+, currently a string

    startTime = db.Column(db.String(40))# Concert start time

    url = db.Column(db.String(200))

    artist_shows=db.relationship("Venue", back_populates='venue_shows')
    venue_shows=db.relationship("Artist", back_populates='artist_shows')

db.drop_all()
db.create_all()
# base_url = "http://localhost:5000/.json"
# response = requests.get(url=base_url)
# json_data = json.loads(response.text)

writeRandomQuote = function () {
    var quotes = new Array();
    quotes[0] = "There's a freedom you begin to feel the closer you get to Austin, Texas. — Willie Nelson.";
    quotes[1] = "There's nothing better than live music. It's raw energy, and raw energy feeds the soul. - Dhani Jones";
    quotes[2] = "I like it here in Austin. Anybody got a room? – Keith Richards, Rolling Stones";
    quotes[3] = "I think that live music is something that the Internet can never kill. — Jim James";
    var rand = Math.floor(Math.random()*quotes.length);
    document.write('<span style="font-family:Monaco,monospace;font-size:25px">'+quotes[rand]+'</span>');
    
  }
 //writeRandomQuote();
 
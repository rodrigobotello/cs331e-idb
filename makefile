FILES :=                              			  \
    main.py                    		  			  \
	models.py                    	  			  \
	create_db.py                      			  \
	models.py                    	  			  \
	tmAPI.py                     	  			  \
	test.py					          			  \
	data.json    			          			  \
	artistData.json       			  			  \
	venueData.json        			  			  \
	genreData.json			          			  \
	requirements.txt			      			  \
	templates/styles.css			  			  \
	templates/index.html			  			  \
	templates/about.html			  			  \
	templates/venues/venue.html		  			  \
	templates/venues/venues_template.html		  \
	templates/musicians/musician.html			  \
	templates/musicians/musician_template.html	  \
	templates/genres/genre.html					  \
	templates/genres/genre_template.html		  \
	templates/search/search.html				  \
	templates/search/search_results.html		  \
	app.yaml									  \

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif

IDB3.log:
	git log > IDB3.log

Models.html: main.py
	$ pydoc3 -w models

TestCollatz.tmp: TestCollatz.py
	$(COVERAGE) run    --branch TestCollatz.py >  TestCollatz.tmp 2>&1
	$(COVERAGE) report -m                      >> TestCollatz.tmp
	cat TestCollatz.tmp


check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "Success!";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -rf __pycache__
	rm -rf sql_db.db

pull:
	$ python3 tmAPI.py

run:
	$ python3 models.py
	$ python3 create_db.py
	$ python3 main.py

unittest:
	$ python3 test.py

test: IDB3.log Models.html check unittest

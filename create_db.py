# Run this after running models.py

import json
from models import app, db, Artist, Genre, Venue, Show

# Insert artist objects in Artist table
def artist():
    data = json.loads(open('artistData.json').read())
    j = 0
    for i in data['artist']:
        artist_id = i['id']
        name = i['name']
        genreID = i['genre']['id']
        subGenreID= i['subgenre']['id']
        socMed = i["externalLinks"]
        images = i["images"]
        totalNumUpcoming=i['upcoming']
        upcomingShows = i['upcomingShows']
        #print(artist_id, name, genreID, subGenreID, socMed, totalNumUpcoming)
        thisArtist = Artist(artist_id=artist_id, name=name)
        for k in upcomingShows:
            show = Show(date=k['showDate'], \
                    venue_id=k['venue_id'], \
                    artist_id=artist_id,\
                    show_id=k['show_id'], \
                    saleStart=k['saleStart'],\
                    presaleStart=k['presaleStart'],\
                    priceMin = k['priceMin'],\
                    startTime = k['startTime'],\
                    doorsOpen = k['doorsOpen'],\
                    restricted = k['restricted'],\
                    url = k['url'])
            thisArtist.artist_shows.append(show)
        thisArtist.totalNumUpcoming = totalNumUpcoming
        thisArtist.images=images
        #thisArtist.upcomingShows = upcomingShows
        #thisArtist.genres = genreID
        thisArtist.socMed= socMed


        # While we're at it, add the first image of the artist to the list of
        # images on the artist's main genre because what else would we use as images of
        # a genre if not artists within it like what would a picture of jazz even look like lol
        genre_images=[]
        thisGenre = db.session.query(Genre).filter(Genre.genre_id==genreID).one()
        thisGenre.images.append(images[0])
        #print(images[0])
        #print(thisGenre.name, thisGenre.images)

        #print(thisGenre.name+":", images)
        db.session.add(thisArtist)
        thisArtist.genres.append(thisGenre)
        db.session.commit()
        #print(thisArtist.genre())

# Insert genre objects in Genre table
def genre():
    data = json.loads(open('genreData.json').read())
    ids=[]
    for i in data['genre']:
        genre_id = i['id']
        name = i['name']
        # Might need to create an additional table for this, idk yet
        # for now it's not actually being included in the db tho
        subGenres=[[j['id'], j['name']] for j in i['_embedded']['subgenres']]
        artists = i['austinArtists']
        if genre_id in ids:
            continue
        thisGenre = Genre(genre_id=genre_id, name=name)
        ids.append(genre_id)
        thisGenre.subgenres = subGenres

        # Initialize list of images to associate with the genre;
        # this will be populated through the artist function.
        thisGenre.images = []

        db.session.add(thisGenre)
        db.session.commit()

# Insert venue objects in Venue table
def venue():
    data = json.loads(open('venueData.json').read())
    ids = []
    for i in data['venue']:
        venue_id = i['id']
        name = i['name']
        post_code=i['postalCode']
        address=i['address']['line1']
        thisVenue = Venue(venue_id=venue_id, name=name, post_code=post_code, address=address)
        thisVenue.images=i['images']
        # print(venue_id+"\n")
        # print(venue_id in ids)
        ids.append(venue_id)
        db.session.add(thisVenue)
        db.session.commit()
    d = db.session.query(Venue)
    venues =[i.name for i in d]
    # for venue in venues:
    #     print(venue+"\n")
    return ids


venue_ids = venue()
genre()
artist()

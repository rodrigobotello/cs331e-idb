# This file accesses the ticketmaster API.
# Uncomment the 'getEvents' call near the bottom to obtain new data.
import ticketpy, json

# The main data. All other JSON files reference this when accessing the API.
eventData = {}
# More specific data. Artist, genre and venue objects.
artistData = {}
genreData = {}
venueData = {}

eventData['event']=[]
artistData['artist']=[]
genreData['genre'] = []
venueData['venue']=[]
genre_ids=[]

# We need to remember to hide this before this goes public
tm_client = ticketpy.ApiClient('KdJVmL1tAsAQ1kIjD0n6kzAqDDnCOGQ6')


# This refreshes the data.json file.
def getEvents():
    event_pages = tm_client.events.find(
        segment_id = "KZFzniwnSyZfZ7v7nJ",
        city='Austin',
        stateCode='TX',
    )
    for page in event_pages:
        for event in page:
            eventData['event'].append(event.json)
    writeData(eventData, 'data')


# The main purpose of this function is to attach the artists within a genre to
# the genre object itself, so it'll be easier to display them.
# I'm still wrestling with this a bit.
def getGenres(genres):
    if genres != {}:
        for k in genre_ids:
            genreObject = tm_client.classifications.genre_by_id(k[1]).json
            artists = []
            for i in genres[k[0]]:
                artist = {'id': i.id, 'name':i.name}
                if artist not in artists:
                    artists.append(artist)
            genreObject.update({'austinArtists': artists})

            genreData['genre'].append(genreObject)


# Write data to JSON files.
def writeData(data, filename):
    with open(filename+'.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def main():
    # Open master file of data
    inf = open('data.json', 'r', encoding='utf-8')
    x = json.load(inf)
    genres = {}

    for i in x["event"]:
        # Try to access artist object via event
        try:
            # Temporary artist dictionary
            thisArtist={}
            artistID = i["_embedded"]["attractions"][0]["id"]
            artistName = i["_embedded"]["attractions"][0]["name"]
            artistObject = tm_client.attractions.find(name=artistName, attraction_id=artistID).one()[0]
            artist = artistObject.json

            thisArtist.update({'id':artistID})
            thisArtist.update({'name': artistName})
            thisArtist.update({'url':artist['url']})

            # There are a lot of try/excepts here because sometimes certain data is missing from the
            # original page source, and most of the time we probably wanna include it anyway
            try:
                artistLinks = artist['externalLinks']
            except:
                artistLinks="This artist doesn't have any external links."
            thisArtist.update({'externalLinks':artistLinks})

            try:
                artistGenre = artist['classifications'][0]['genre']
            except:
                artistGenre = "Undefined"
            thisArtist.update({'genre':artistGenre})

            try:
                artistSubGenre = artist['classifications'][0]['subGenre']
            except:
                artistSubGenre = "Undefined"
            thisArtist.update({'subgenre':artistSubGenre})

            try:
                images = artist['images']
            except:
                images = 'none'
            thisArtist.update({'images':images})

            # Create a dictionary for the show. Might wanna move this to a
            # separate function later, for better readability
            thisShow = {'showDate':i["dates"]["start"]["localDate"], \
            'venue_id':i["_embedded"]["venues"][0]["id"], \
            'show_id':i['id'],\
            'url':i['url']}
            try:
                presaleStart= i['sales']['presales'][0]['startDateTime']
            except:
                presaleStart = "This event does not have presales."

            try:
                priceMin = i['priceRanges'][0]['min'] + i['priceRanges'][0]['currency']
            except:
                priceMin = 'Unknown'

            try:
                restricted = i['ageRestrictions']['legalAgeEnforced']
            except:
                restricted = 'Unknown'
            try:
                doorsOpen = i['doorsTimes']['localTime']
            except:
                doorsOpen = 'Unknown'

            try:
                saleStart=i['sales']['public']['startDateTime']
            except:
                saleStart='Unknown'

            try:
                startTime=i['dates']['start']['localTime']
            except:
                startTime='Unknown'
            thisShow.update({'presaleStart':presaleStart})
            thisShow.update({'priceMin':priceMin})
            thisShow.update({'restricted':restricted})
            thisShow.update({'doorsOpen':doorsOpen})
            thisShow.update({'saleStart':saleStart})
            thisShow.update({'startTime':startTime})

            # Add the show dictionary
            thisArtist.update({'upcomingShows':[thisShow]})

            # Total number of events the artist has planned, regardless of location.
            thisArtist.update({'upcoming':artist['upcomingEvents']['_total']})

            # If the artist already appears in the dictionary, it's because there is more than one show in Austin.
            # In this case, just add the new show to the list upcomingShows to avoid repetition.
            if artistData['artist']!=[]:
                if artistData['artist'][-1]['id']==thisArtist['id']:
                    artistData['artist'][-1]['upcomingShows'].append(thisArtist['upcomingShows'][0])
                    continue
            artistData['artist'].append(thisArtist)
        except:
        # If it doesn't work, screw it, just skip it.
            continue

        # Temporary venue dictionary
        thisVenue={}
        venueID = i["_embedded"]["venues"][0]["id"]
        venueName = i["_embedded"]["venues"][0]["name"]

        venueObject = tm_client.venues.find(keyword=venueName, venue_id=venueID)


        # For some reason certain venues don't retrieve a venue object;
        # putting this here until i can figure out if it's my mistake or just
        # a flaw in the data)
        try:
            venue = venueObject.one()[0].json
            thisVenue.update({'id':venueID})
            thisVenue.update({'name':venueName})
            thisVenue.update({'upcomingShows':[{'show_id':i['id'], 'artist':thisArtist['name']}]})

            # If the venue already appears in venueData['venue'], it's because there
            # are multiple shows there, which is normal. In this case, we want to append
            # the current show to the original entry instead of repeating it.
            alreadyExists=False
            for k in venueData['venue']:
                if thisVenue['id']==k['id']:
                    # It really do be existin
                    alreadyExists=True

                    # Append the current show to that venue's upcomingShows list
                    k['upcomingShows'].append(thisVenue['upcomingShows'][0])

                    break

            # If alreadyExists is still false, the venue isn't in venueData yet,
            # so we can go ahead and gather the rest of the data and add it into venueData
            if not alreadyExists:
                thisVenue.update({'url':venue['url']})
                try:
                    thisVenue.update({'aliases':venue['aliases']})
                except:
                    thisVenue.update({'aliases':None})


                thisVenue.update({'postalCode':venue['postalCode']})
                thisVenue.update({'address':venue['address']})
                thisVenue.update({'upcoming':venue['upcomingEvents']['_total']})
                try:
                    images = venue['images']
                except:
                    images = 'none'
                thisVenue.update({'images':images})

                venueData['venue'].append(thisVenue)

        except:
            venueObject = venueObject


        genre = i["_embedded"]["attractions"][0]["classifications"][0]["genre"]
        subgenre = i["_embedded"]["attractions"][0]["classifications"][0]["subGenre"]

        if genre["name"] not in genres:
            genres[genre["name"]]=[]
            genre_ids.append([genre["name"],genre["id"]])
            genre_ids.append([subgenre["name"],subgenre["id"]])

        if subgenre["name"] not in genres:
            genres[subgenre["name"]]=[]
            genre_ids.append([subgenre["name"],subgenre["id"]])

        genres[genre["name"]].append(artistObject)
        genres[subgenre["name"]].append(artistObject)

        # for testing purposes
        # if idx ==2:
        #     break
        # idx+=1
    getGenres(genres)
    writeData(artistData, "artistData")
    writeData(venueData, "venueData")
    writeData(genreData,"genreData")

# Uncommenting this will cause the program to access and write new data to the data.json file.
# We should set some sort of regular update for this, maybe every day at a certain time.
#getEvents()
main()
